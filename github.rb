module GitHub
  class Blob
    def initialize tree, info
      @tree = tree
      @info = info
    end

    def github_url
      "/blob/#{@tree.ref}/#{@info[:path]}"
    end
  end

  class Tree
    attr_reader :repo
    attr_reader :ref

    @@trees = {}

    def initialize repo, ref
      @repo = repo.gsub(/\W/, '')
      @ref = ref.gsub(/\W/, '')
      @tree = nil
      @last_update = 0
    end

    def fresh?
      (DateTime.now.to_time.to_i - @last_update) < 3600
    end

    def valid?
      return (@tree.kind_of?(Array) and fresh?)
    end

    def find_blob pattern
      @tree.each do |blob|
        next unless pattern.match(blob[:path])
        return Blob.new(self, blob)
      end
    end

    def self.get repo, ref
      key = "#{repo}@#{ref}"

      if @@trees.has_key?(key)
        tree = @@trees[key]
      else
        tree = Tree.new(repo, ref)
        @@trees[key] = tree
      end

      tree.update unless tree.fresh?
      tree
    end

    def update
      uri = URI("https://api.github.com/repos/axr/#{@repo}/git/trees/#{@ref}?recursive=1")
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      response = http.request(Net::HTTP::Get.new(uri.request_uri))

      begin
        data = JSON.parse(response.body, {
          :symbolize_names => true
        })
      rescue JSON::ParserError
        data = nil
      end

      @tree = data.has_key?(:tree) ? data[:tree] : nil
      @last_update = DateTime.now.to_time.to_i
    end
  end
end

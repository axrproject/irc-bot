require 'cinch'
require 'digest/sha1'
require 'json'
require 'net/http'
require_relative 'github'

class GithubFilesPlugin
  include Cinch::Plugin

  @@no = {}

  set :prefix, //
  match /^(.+)$/i

  def execute (m, message)
    /^!(ghfile|gf)\s+(ref:(?<ref>\w+)\s+)?(?<file>.+)$/.match(message) do |match|
      tree = GitHub::Tree.get('core', match[:ref] || 'master')

      if tree.valid?
        blob = tree.find_blob(/(\A|\/)#{Regexp.escape match[:file]}$/)

        unless blob.nil?
          m.reply("https://github.com/axr/core#{blob.github_url}")
          return
        end
      end

      hash = Digest::SHA1.hexdigest message
      if !@@no.has_key?(hash) or (DateTime.now - @@no[hash][:time]) > 300
        @@no[hash] = {
          :count => 0,
          :time => DateTime.now
        }
      end

      @@no[hash][:count] += 1
      @@no[hash][:time] = DateTime.now

      if @@no[hash][:count] == 3
        m.reply("#{m.user.nick}: No means no, dammit!")
      elsif @@no[hash][:count] < 3
        m.reply("#{m.user.nick}: No.")
      end
    end
  end
end

require 'cinch'
require 'json'
require 'net/http'

class XKCDPlugin
  include Cinch::Plugin

  set :prefix, //
  match /^(.+)$/i

  def execute (m, message)
    message.scan(/xkcd#(\d+)/).each do |item|
      comic_id = item[0]
      url = comic_link(comic_id)

      return unless comic_exists? comic_id

      m.reply(comic_link(comic_id))
    end
  end

  def comic_link (id)
    return "https://xkcd.com/#{id}/"
  end

  def comic_exists? (id)
    url = URI.parse(comic_link(id))
    request = Net::HTTP.new(url.host, url.port)
    request.use_ssl = true
    response = request.request_head(url.path)

    return response.code == "200"
  end
end

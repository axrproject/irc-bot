require 'cinch'
require 'json'
require 'net/http'

class GithubIssuesPlugin
  include Cinch::Plugin

  @@repos = ["core", "common", "plugin", "browser", "examples", "specification",
    "website", "website-data", "framework", "extras"]
  @@ignored_nicks = ["AXR|GitHub", "travis-ci"]

  set :prefix, //
  match /^(.+)$/i

  def execute (m, message)
    message.scan(/(^|\s)([a-zA-Z0-9\-_]+)?#(\d+)/).each do |item|
      repo = get_repo_name item[1]
      issue = item[2]

      return if repo.nil? or repo.empty?
      return if @@ignored_nicks.include? m.user.nick

      summary  = issue_summary(repo, issue)

      unless summary.nil?
        m.reply(summary)
        m.reply(issue_link(repo, issue))
      end
    end
  end

  def issue_summary (repo, issue)
    data = get_issue_info(repo, issue)
    return if data.nil?

    out = "##{issue} \"#{data["title"]}\""

    if data["assignee"] != nil
      out += " for #{data["assignee"]["login"]}"
    end

    if data["closed_at"] != nil
      out += " [CLOSED]"
    end

    return out
  end

  def issue_link (repo, issue)
    return "https://github.com/axr/#{repo}/issues/#{issue}"
  end

  def get_repo_name (repo)
     if repo.nil?
      return
    end

    if not @@repos.include? repo.downcase
      matches = @@repos.select { |item| item.index(repo) == 0 }

      return unless matches.length > 0
      repo = matches[0]
    end

    return repo
  end

  def get_issue_info (repo, issue)
    uri = URI("https://api.github.com/repos/axr/#{repo}/issues/#{issue}")
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    response = http.request(Net::HTTP::Get.new(uri.request_uri))

    data = JSON.parse(response.body)

    if data["message"] != nil
      return nil
    end

    return data
  end
end


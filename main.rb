#!/usr/bin/ruby

require 'cinch'

require_relative 'github_issues_plugin'
require_relative 'github_files_plugin'
require_relative 'xkcd_plugin'
require_relative 'logger_plugin'

bot = Cinch::Bot.new do
  configure do |c|
    c.server = "irc.freenode.net"
    c.nick = "Axerr"
    c.channels = ["#axr"]
    c.plugins.plugins = [
      GithubIssuesPlugin,
      GithubFilesPlugin,
      XKCDPlugin,
      LoggerPlugin
    ]
  end
end

bot.start

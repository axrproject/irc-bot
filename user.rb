class User
  attr_accessor nick

  def initialize (nick)
    @nick = nick

    register_activity
  end

  def register_activity
    @last_activity_time = DateTime.now
  end
end


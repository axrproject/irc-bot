require 'cinch'
require 'date'
require 'iconv'

class LoggerPlugin
  include Cinch::Plugin

  @@logdir = "/var/dev/files/irc"

  listen_to :catchall

  def listen m
    m.raw.match(/^:(?<nick>.+?) JOIN (?<channel>#.+)$/) do |match|
      _append("#{_nick(match["nick"])} has just joined #{match["channel"]}")
      return
    end

    m.raw.match(/^:(?<nick>.+?) PART (?<channel>#.+?) :(?<message>.+)$/) do |match|
      _append("#{_nick(match["nick"])} has just parted: #{match["message"]}")
      return
    end

    m.raw.match(/^:(?<nick>.+?) QUIT :(?<message>.+)$/) do |match|
      _append("#{_nick(match["nick"])} has just quit: #{match["message"]}")
      return
    end

    m.raw.match(/^:(?<nick>.+?) NICK :(?<new>.+)$/) do |match|
      _append("#{_nick(match["nick"])} is now known as #{match["new"]}")
      return
    end

    m.raw.match(/^:(?<nick>.+?) TOPIC .+? :(?<message>.+)$/) do |match|
      _append("#{_nick(match["nick"])} has just changed the topic to \"#{match["message"]}\"")
      return
    end

    m.raw.match(/^:(?<nick>.+?) PRIVMSG .+? :(?<message>.+)$/) do |match|
      action = match["message"].sub(/^\x01ACTION (.+)\x01$/, "\\1")

      if action == match["message"]
        _append("<#{_nick(match["nick"])}> #{match["message"]}")
      else
        _append("#{_nick(match["nick"])} #{action}")
      end

      return
    end

    m.raw.match(/^:(?<nick>.+?) NOTICE .+? :(?<message>.+)$/) do |match|
      _append("Notice(#{_nick(match["nick"])}): #{match["message"]}")
      return
    end
  end

  def _nick (nick)
    nick.split('!')[0]
  end

  def _append (line)
    file = "#{@@logdir}/axr_#{Date.today.to_s}.log"
    line = Iconv.new('UTF-8//IGNORE', 'UTF-8').iconv(line + ' ')[0..-2]
    line = line.gsub('\x0F', '')

    open(file, "a") do |f|
      f.puts "#{DateTime.now.to_s} #{line}"
    end
  end
end

